// #define __DEBUG__ 4
// #ifndef __MODULE__
// 	#define __MODULE__ "app_utils.c"
// #endif
// #include "user_debug.h"
#include "app_conf.h"
// #include "app_utils.h"
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>

// HAL dependancies
// #include "stm32f2xx_hal.h"
// Relate to Task utils
// #include "cmsis_os.h"

/*
void Task_Error_Handler(const char *file, uint32_t line)
{
	debug("Task Error:\nFile: %s\nLine: %d\n", file, line);
	while(1);
}

void Task_ShowStack(void)
{
	debug("MrHome_ShowStack:\n"); 
	debug("Free heap size: %lu\n", xPortGetFreeHeapSize());
	debug("Free stack size: %lu\n\r", uxTaskGetStackHighWaterMark( NULL ));
}
*/
size_t strnlen (const char* s, size_t maxlen)
{
	size_t len = 0;

	while ((len < maxlen) && (*s))
  {
	  s++;
	  len++;
  }

	return len;
}

/*
int string_split_checksize(char *input, char splitter)
{
	int n = 0;
	bool c = false;
	while(*input){
		if (*input == splitter){
			if (c){
				n++;
			}		
			c = false;
		}
		else {
			c = true;
		}
		input++;
	}
	if (c) n++;
	return n;
}

// v3:
// return: num of split
int string_split(char *input, PART_T *pieces, int maxPiece, char splitter)
{	
	int n = 0;
	bool c = false;
	PART_T *p = pieces;
	while(*input){
		if (*input == splitter){
			if (c){
				p[n].len = input - p[n].part;
				n++;
			}
			*input = 0;
			c = false;
			if (n == maxPiece) return n;
		}
		else {
			if (!c){
				p[n].part = input;
			}
			c = true;
		}
		input++;
	}
	if (c){
		p[n].len = input - p[n].part;
		n++;
	}
	return n;
}

void string_to_upper(char *input)
{
	int len = strlen(input);
	for (int i = 0; i < len; i++) {
		if (islower(input[i])) input[i] = toupper(input[i]);
	}
}
*/

int str_token(char *s, char *set, char **token, int maxSz)
{
	char *tok;
	int count = 0;
  tok = strtok (s, set);
  while (tok != NULL) {
    if (count < maxSz){
    	*token++ = tok;	
    	count++;
    }
    tok = strtok (NULL, set);
  }

  return count;
}


/****** us delay ********/
/*
void DWT_Init(void) 
{
  if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) 
  {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  }
}
 
void DWT_Delay(uint32_t us) // microseconds
{
  uint32_t tp = DWT->CYCCNT + us * (SystemCoreClock/1000000);
  while (DWT->CYCCNT < tp);
}
*/

bool timeout_check(uint32_t *from, uint32_t interval)
{
	if (millis() - (*from) >= interval){
		*from = millis();
		return true;
	}
	return false;
}

// void dummy_packet(uint8_t packet[], int len)
// {
// 	for (int i=0;i<len;i++){
// 		debugx("%02X ", packet[i]);
// 	}
// }
