
#ifndef __APP_UTILS_H__
#define __APP_UTILS_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
	 
// typedef struct __PART_T {
// 	char *part;
// 	int len;
// } PART_T;

// void Task_Error_Handler(const char * file, uint32_t line);
// void Task_ShowStack(void);
size_t strnlen(const char * s, size_t maxlen);
// v3:
// int string_split_checksize(char * input, char splitter);
// int string_split(char *input, PART_T *pieces, int maxPieces, char splitter);
// void string_split_free(PART_T *pieces);
// void string_to_upper(char *input);

// New token utils
int str_token(char *s, char *set, char **token, int maxSz);

// delayus
//void DWT_Init(void);
//void DWT_Delay(uint32_t us);

bool timeout_check(uint32_t *from, uint32_t interval);
#define timeout_init(timeout) (*timeout) = millis()

// void dummy_packet(uint8_t packet[], int len);

#ifdef __cplusplus
}
#endif

#endif

