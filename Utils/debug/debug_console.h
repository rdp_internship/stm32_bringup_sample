#ifndef __DEBUG_CONSOLE_H_
#define __DEBUG_CONSOLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>

// Utils
#define __check_cmd_at_pos(x,p) (strncmp(&result[p], x, strlen(x)) == 0)
#define __check_cmd(x) __check_cmd_at_pos(x,0)
#define __param_pos(x)	((char *)(&result[strlen(x)]))

int console_read(char **result);
int console_read_timeout(char **result);

#ifdef __cplusplus
}
#endif

#endif
