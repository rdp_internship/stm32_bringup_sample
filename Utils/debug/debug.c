/**
	Standard debug utils
	
	MinhTh
	12/7/2016
**/

#include "debug.h"
#include <stdarg.h>
#include <stdio.h>

#include "hm_serial_uart_conf.h"
#include "hm_serial_uart8.h"

#define DEBUG_BUFF_MAX_SIZE 512

// Sem/mutex lock
//SemaphoreHandle_t dbgSem_id;
static uint8_t buffer[DEBUG_BUFF_MAX_SIZE];
static uint8_t debug_user_serial_uart_tx_bufs[DEBUG_BUFF_MAX_SIZE];
static uint8_t debug_user_serial_uart_rx_bufs[DEBUG_BUFF_MAX_SIZE];
HmSerialUart8Handle_t debug_user_serial_uart_handle;

// thay the debug neu muon
// function user_debug_init() : khoi dong debug
// function uart_printf(): chinh sua ham xuat ra ngoai
// function uart_vprintf(): chinh sau ham xuat ra ngoai

void user_debug_init(void)
{
//  init lop duoi o day
//  hoac la debug imt
//  osSemaphoreDef(DBGSEM);
//  dbgSem_id = osSemaphoreCreate(osSemaphore(DBGSEM), 1);
	
	// danh cho module sim
	// sensor debug: usart2;
	// main: usart1;
	HmSerialUart8_Init(&debug_user_serial_uart_handle);
	HmSerialUart8_Registry(&debug_user_serial_uart_handle,
									DEBUG_CONSOLE_UART_HANDLE, HM_SERIAL_UART8_RX_DMA_MODE | HM_SERIAL_UART8_TX_DMA_MODE,
									debug_user_serial_uart_tx_bufs, DEBUG_BUFF_MAX_SIZE, 
									debug_user_serial_uart_rx_bufs, DEBUG_BUFF_MAX_SIZE,
									NULL);
	
	HmSerialUart8_Start(&debug_user_serial_uart_handle);
}

static void uart_printf(const char *fmt, ...)
{
	uint32_t len;
	va_list vArgs;
	va_start(vArgs, fmt);
	len = vsprintf((char *)&buffer[0], (char const *)fmt, vArgs);
	va_end(vArgs);

	// xuat du lieu ra ben ngoai
	HmSerialUart8_Transmit(&debug_user_serial_uart_handle, buffer, len);
}

// chinh sua cai nay
static void uart_vprintf(const char *fmt, va_list vArgs)
{
	uint32_t len;
	len = vsprintf((char *)&buffer[0], (char const *)fmt, vArgs);

	HmSerialUart8_Transmit(&debug_user_serial_uart_handle, buffer, len);
}

/* ############### Actual debug redirect ################# */
#define __debug_printf(fmt, ...) uart_printf(fmt, __VA_ARGS__)
#define __debug_vprintf(fmt, vArgs) uart_vprintf(fmt, vArgs)

/* ------------------- MAIN DEBUG I/O -------------- */
void user_debug_print(int level, const char* module, int line, const char* fmt, ...)
{
//	osSemaphoreWait(dbgSem_id , osWaitForever);
	//__debug_printf("->%s | line %d: ", module, line);
	
	switch (level){
		case 1:
			// "->[ERROR](module:line): "
			__debug_printf("->[ERROR](%s:%d): ", module, line);
			break;
		case 2:
			// "->[WARN](module:line): "
			__debug_printf("->[WARN](%s:%d): ", module, line);
			break;
		case 3:
			// "->[INFO](module:line): "
			__debug_printf("->[INFO](%s:%d): ", module, line);
			break;
		case 4:
		default:
			// Don't need to print DEBUG :P
			__debug_printf("->(%s:%d): ", module, line);
			break;
	}
	
	va_list     vArgs;		    
	va_start(vArgs, fmt);
	__debug_vprintf((char const *)fmt, vArgs);
	va_end(vArgs);

//	osSemaphoreRelease(dbgSem_id);
}

void user_debug_print_error(const char* module, int line, int ret)
{
//	osSemaphoreWait(dbgSem_id , osWaitForever);
	__debug_printf("[RC] Module %s - Line %d : Error %d\n", module, line, ret);
//	osSemaphoreRelease(dbgSem_id);
}

void user_debug_print_exact(const char* fmt, ...)
{
//	osSemaphoreWait(dbgSem_id , osWaitForever);

	va_list     vArgs;		    
	va_start(vArgs, fmt);
	__debug_vprintf((char const *)fmt, vArgs);
	va_end(vArgs);

//	osSemaphoreRelease(dbgSem_id);
}

