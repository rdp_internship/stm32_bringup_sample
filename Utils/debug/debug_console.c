
#include "debug_console.h"
#include "hm_serial_uart_conf.h"
#include "stdint.h"
#include "hm_serial_uart8.h"

// include not include

#define CONSOLE_BUFFER_MAX_SIZE 512
static char console_buffers[CONSOLE_BUFFER_MAX_SIZE];
static int console_state = 0;
static int console_timeout = 0;
static int console_index = 0;

// thay the neu muon
// lien quan den debug_user.h
extern HmSerialUart8Handle_t debug_user_serial_uart_handle;
#define __CONSOLE_MILLIS() HAL_GetTick()
#define __CONSOLE_AVAILABLE() (HmSerialUart8_RxAvailable(&debug_user_serial_uart_handle) != 0)
#define __CONSOLE_RECEIVE(x,y) HmSerialUart8_Receive(&debug_user_serial_uart_handle, x, y)
#define __CONSOLE_FLUSH() HmSerialUart8_RxFlush(&debug_user_serial_uart_handle)

int console_read_timeout(char **result)
{
	switch(console_state)
	{
		case 0:
		{
			if(__CONSOLE_AVAILABLE())
			{
				console_state = 1;
				console_index = 0;
				console_timeout = __CONSOLE_MILLIS();
			}
		}
		break;

		case 1:
		{
			if(__CONSOLE_MILLIS() - console_timeout < 20)
			{
				if(__CONSOLE_AVAILABLE())
				{
					console_timeout = __CONSOLE_MILLIS();
					while (__CONSOLE_AVAILABLE())
					{
						uint8_t in_char;
						uint32_t len = 1;

						__CONSOLE_RECEIVE(&in_char, &len);
						console_buffers[console_index++] = in_char;
					}
				}
			}
			else
			{
				console_buffers[console_index] = '\0';
				int len = console_index;
				console_index = 0;
				console_state = 0;
				
				*result = console_buffers;
				return len;
			}
		}
		break;
	}

	return 0;
}

int console_read(char **result)
{
  	static int i = 0;
	while (__CONSOLE_AVAILABLE())
	{
		uint8_t in_char;
		uint32_t len = 1;
		
		__CONSOLE_RECEIVE(&in_char, &len);
		if (in_char == '\r')
		{
			console_buffers[i] = '\0';
			len = i;
			i = 0;
			*result = console_buffers;
			__CONSOLE_FLUSH();
			return len;
		}
		else if (in_char != '\n')
		{
			console_buffers[i] = in_char;
			i++;
			 
			if (i == (CONSOLE_BUFFER_MAX_SIZE-1))
			{
				console_buffers[i] = '\0';
				len = i;
				i = 0;
				*result = console_buffers;
				__CONSOLE_FLUSH();
				return len;
			}
		}
	}
	return 0;
}
