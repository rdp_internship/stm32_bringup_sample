#ifndef __APP_CONF_H_
#define __APP_CONF_H_

#ifdef __cplusplus
extern "C" {
#endif

// types
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>

#define TRUE true
#define FALSE false
	
// os
// #include "cmsis_os.h"

// HieuNT: add for convenient
#define _size(x) (sizeof(x)-1)
// #define _malloc(x) (pvPortMalloc(x))
// #define _free(x) 		(vPortFree(x))
#define delay(x) HAL_Delay(x) // osDelay(x)

// HAL specific
uint32_t HAL_GetTick(void);
#define millis() HAL_GetTick()
// void DWT_Delay(uint32_t us);
// #define delay_us(us) DWT_Delay(us)

#define UART_DEBUG (&huart1_dma)

#ifdef __cplusplus
}
#endif

#endif
