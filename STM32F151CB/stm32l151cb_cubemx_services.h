/**
	Header file for STM32L151CB cubemx code gen. with HieuNT I/O services.

	HieuNT
	13/12/2016
*/

#ifndef __SI4463_STM32L151CB_APP_H_
#define __SI4463_STM32L151CB_APP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32l1xx_hal.h"
#include "dma.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

// HieuNT services
#include "app_conf.h"
#include "debug_user.h"
#include "debug_console.h"
#include "main.h"
	
#ifdef __cplusplus
}
#endif

#endif
