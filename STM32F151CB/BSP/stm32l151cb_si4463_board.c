/*
	BSP (Board support package)
	For HieuNT STM32L151CB and Si4463 board

	HieuNT
	11/8/2016
*/

//#include "stm32l151cb_cubemx_services.h"
#include "stm32l1xx_hal.h"
#include "dma.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

#include "main.h"

#include "stm32l151cb_si4463_board.h"

extern uint32_t halTimerIrqCount;
__IO bool pirExtIntActive = false;
__IO bool btnExtIntActive = false;

void HAL_SYSTICK_Callback(void)
{
  // halTimerIrqCount++;
}

void BSP_IO_Init(void)
{
	// UART: done in cubemx and service
	// Timer base: done in HAL
	// PWM: N/A
	// User button via Ext IRQ: done in cubemx
	// EEPROM: TODO!
	BSP_Si446x_CS_HIGH();
	BSP_Si446x_ShutDown();
	HAL_Delay(100);
}

/* ============== Hardware SPI =================== */

// SPI Init & DeInit: done in cubemx


/**
  * @brief  Sends a byte through the SPI interface and return the byte received
  *         from the SPI bus.
  * @param  byte: byte to send.
  * @retval The value of the received byte.
  */
uint8_t BSP_SPI_ReadWrite(uint8_t byte)
{
	uint8_t byteOut;
  HAL_SPI_TransmitReceive(&hspi1, &byte, &byteOut, 1, 1);
	return byteOut;
}

void BSP_SPI_WriteData(uint8_t byteCount, uint8_t *pData)
{
  uint8_t i;
  //for (i = 0; i < byteCount; i++)
  //  BSP_SPI_ReadWrite(*pData++);
  HAL_SPI_Transmit(&hspi1, pData, byteCount, byteCount);
	
  // TODO: block and DMA
}

void BSP_SPI_ReadData(uint8_t byteCount, uint8_t *pData)
{
  uint8_t i;
  //for (i = 0; i < byteCount; i++)
  //  *pData++ = BSP_SPI_ReadWrite(0xff);
  HAL_SPI_Receive(&hspi1, pData, byteCount, byteCount);
  
  // TODO: Block and DMA
}
