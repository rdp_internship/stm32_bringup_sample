/**
  ******************************************************************************
  * @file    hm_serial_uart.h
  * @author  Hoang Minh
  * @version V1.0.2
  * @date    23-November-2016
  * @brief   Header file of Hm_Serial_Uart module.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HM_SERIAL_UART_CONF_H
#define __HM_SERIAL_UART_CONF_H

// thay doi cai nay ung voi moi loai chip
#include "stm32l1xx_hal.h"
#include "stdint.h"
#include "usart.h"

#define DEBUG_CONSOLE_UART_HANDLE &huart1

#endif

