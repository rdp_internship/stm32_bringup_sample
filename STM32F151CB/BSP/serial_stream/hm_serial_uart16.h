/**
  ******************************************************************************
  * @file    HM_SERIAL8_uart.h
  * @author  Hoang Minh
  * @version V1.0.2
  * @date    23-November-2016
  * @brief   Header file of HM_SERIAL8_Uart module.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************  
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HM_SERIAL_UART16_H_
#define __HM_SERIAL_UART16_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "hm_serial_uart_conf.h"

#define HM_SERIAL_UART16_TX_DMA_MODE 		(1 << 0)
#define HM_SERIAL_UART16_TX_IT_MODE			(1 << 1)
#define HM_SERIAL_UART16_TX_NORMAL_MODE  (1 << 2)
#define HM_SERIAL_UART16_RX_DMA_MODE	(1 << 3)
#define HM_SERIAL_UART16_RX_IT_MODE		(1 << 4)
#define HM_SERIAL_UART16_RX_NORMAL_MODE  (1 << 5)

/**
  * @brief _HmSerialUart16Handle_t State structures definition
  */
enum
{
	HM_SERIAL_UART16_TX_READY = 0,
	HM_SERIAL_UART16_TX_BUSY
};

enum
{
	HM_SERIAL_UART16_STATUS_OK = 0,
	HM_SERIAL_UART16_STATUS_ERROR,
	HM_SERIAL_UART16_STATUS_TIMEOUT,
	HM_SERIAL_UART16_STATUS_BUSY
};

enum
{
	HM_SERIAL_UART16_RX_WAIT_STATE_IDLE,
	HM_SERIAL_UART16_RX_WAIT_STATE_1,
	HM_SERIAL_UART16_RX_WAIT_STATE_2
};

#define HM_SERIAL_UART16_RX_FULL_EVENT   	(1 << 0)
#define HM_SERIAL_UART16_RX_TIMEOUT_EVENT  (1 << 1)
#define HM_SERIAL_UART16_TX_FULL_EVENT   	(1 << 2)
#define HM_SERIAL_UART16_TX_EMPTY_EVENT 	  (1 << 3)

/* Exported types ------------------------------------------------------------*/
/** @defgroup _HmSerialUart16Handle_t Exported Types
  * @{
  */
  
typedef struct _HmSerialUart16Handle_t HmSerialUart16Handle_t;
typedef uint8_t (*HmSerialUart16CB_t)(struct _HmSerialUart16Handle_t* phandle, uint8_t event);

/**
  * @brief _HmSerialUart16Handle_t Init Structure definition
  */
struct _HmSerialUart16Handle_t
{
	uint8_t mode;
	uint16_t events;
	
	uint16_t tx_tail;
	uint16_t tx_head;
	uint16_t *tx_bufs;
	uint16_t tx_max_size;
	uint16_t tx_size;
	uint16_t tx_state;
	
	uint16_t rx_tail;
	uint16_t rx_head;
	uint16_t *rx_bufs;
	uint16_t rx_max_size;

	uint8_t rx_wait_state;
	uint32_t rx_wait_time_0;
	uint32_t rx_wait_time_1;
	uint32_t rx_wait_tick;
	uint32_t rx_wait_count;

	UART_HandleTypeDef *huart;
	
	void* pdata;
	HmSerialUart16CB_t cb_func;
};

/* Exported functions --------------------------------------------------------*/

/* Initialization and de-initialization functions  ****************************/
void HmSerialUart16_Init(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_DeInit(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_Registry(HmSerialUart16Handle_t *phandle,
									UART_HandleTypeDef *phuart, uint8_t mode,
									void* ptx_buff, uint16_t tx_max_size, 
									void* prx_buff, uint16_t rx_max_size,
									uint8_t (*cb_func) (struct _HmSerialUart16Handle_t*, uint8_t));
void HmSerialUart16_Start(HmSerialUart16Handle_t* phandle);

/* IO operation functions *****************************************************/
void HmSerialUart16_RxFlush(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_TxFlush(HmSerialUart16Handle_t* phandle);
uint32_t HmSerialUart16_RxAvailable(HmSerialUart16Handle_t* phandle);
uint32_t HmSerialUart16_Receive(HmSerialUart16Handle_t* phandle, void *p, uint32_t *plen);
uint32_t HmSerialUart16_Transmit(HmSerialUart16Handle_t* phandle, void *p, uint32_t len);
uint32_t HmSerialUart16_WaitForReceive(HmSerialUart16Handle_t* phandle, uint32_t  time0, uint32_t time1);
void HmSerialUart16_BackGroundProcess(HmSerialUart16Handle_t* phandle);

void HmSerialUart16_StartReceiveEvent(HmSerialUart16Handle_t *phandle, uint32_t time0, uint32_t time1);
uint32_t HmSerialUart16_WaitForReceiveEvent(HmSerialUart16Handle_t *phandle);

/* Link Driver */
void HmSerialUart16_TxCpltCb(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_ErrorCb(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_RxCpltCb(HmSerialUart16Handle_t *phandle);

void HmSerialUart16_LL_MspInit(HmSerialUart16Handle_t* phandle);
void HmSerialUart16_LL_MspTxStart(HmSerialUart16Handle_t* phandle, uint8_t en);
void HmSerialUart16_LL_MspDeInit(HmSerialUart16Handle_t* phandle);

#ifdef __cplusplus
 }
#endif
 
#endif

