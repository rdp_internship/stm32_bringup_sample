
#include "hm_serial_uart16.h"
#include <string.h>

#ifndef __HAL_DMA_GET_COUNTER
	#define __HAL_DMA_GET_COUNTER(__HANDLE__) ((__HANDLE__)->Instance->CNDTR)
#endif

#define HM_SERIAL_UART16_RX_LEN(x) (((x)->rx_tail + (x)->rx_max_size - (x)->rx_head) % (x)->rx_max_size)

#define HM_SERIAL_UART16_TX_LEN(x) (((x)->tx_tail + (x)->tx_max_size - (x)->tx_head) % (x)->tx_max_size)

#define HM_SERIAL_UART16_TX_ALIGNED_LEN(x) ((((x)->tx_tail > (x)->tx_head)) ? ((x)->tx_tail - (x)->tx_head):((x)->tx_max_size - (x)->tx_head))

static uint32_t hmSerialUartContinueTx(HmSerialUart16Handle_t *phandle);

__weak void HmSerialUart16_LL_MspInit(HmSerialUart16Handle_t *phandle)
{

}

__weak void HmSerialUart16_LL_MspTxStart(HmSerialUart16Handle_t* phandle, uint8_t en)
{
	
}

__weak void HmSerialUart16_LL_MspDeInit(HmSerialUart16Handle_t *phandle)
{

}

__weak uint32_t HmSerialUart16_GetTick()
{
	return HAL_GetTick();
}

__weak void HmSerialUart16_Delay(uint32_t tick)
{
	HAL_Delay(tick);
}

static uint32_t HmSerialUart16_GetDeltaTick(uint32_t tick)
{
	return ((HAL_GetTick() >= tick) ? (HAL_GetTick() - tick) : (0xffffffff - tick + HAL_GetTick()));
}

/**
  * @brief  HmSerialUart16_Init
  * @param  None
  * @retval None
  */
void HmSerialUart16_Init(HmSerialUart16Handle_t *phandle)
{
	memset(phandle, 0, sizeof(HmSerialUart16Handle_t));
	
	//Init
	HmSerialUart16_LL_MspInit(phandle);
}

/**
  * @brief  HmSerialUart16_Start
  * @param  None
  * @retval None
  */
void HmSerialUart16_DeInit(HmSerialUart16Handle_t *phandle)
{
	if(phandle == NULL || phandle->huart == NULL)
	{
		return;
	}
	
	/* Del Init */
	HmSerialUart16_LL_MspDeInit(phandle);
	
	memset(phandle, 0, sizeof(HmSerialUart16Handle_t));
}

/**
  * @brief  HmSerialUart16_Registry
  * @param  None
  * @retval None
  */
void HmSerialUart16_Registry(HmSerialUart16Handle_t *phandle,
									UART_HandleTypeDef *phuart, uint8_t mode,
									void* ptx_buff, uint16_t tx_max_size, 
									void* prx_buff, uint16_t rx_max_size,
									uint8_t (*cb_func) (struct _HmSerialUart16Handle_t*, uint8_t))
{
	if(phandle != NULL)
	{
		phandle->mode = mode;
		phandle->huart = phuart;
		phuart->pData = phandle;
		
		phandle->tx_bufs = ptx_buff;
		phandle->tx_max_size = tx_max_size;
		
		phandle->rx_bufs = prx_buff;
		phandle->rx_max_size = rx_max_size;
		
		phandle->cb_func = cb_func;
	}
}

/**
  * @brief  HmSerialUart16_Start
  * @param  None
  * @retval BUSY: Buffer full, Error:, OK
  */
void HmSerialUart16_Start(HmSerialUart16Handle_t *phandle)
{
	if(phandle != NULL && phandle->huart != NULL)
	{
		if(phandle->rx_bufs != NULL)
		{
			HmSerialUart16_LL_MspTxStart(phandle, 0);
			if(phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
			{
				HAL_UART_Receive_IT(phandle->huart, (uint8_t*)&phandle->rx_bufs[0], 1);
			}
			else if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE)
			{
				HAL_UART_Receive_DMA(phandle->huart, (uint8_t*)&phandle->rx_bufs[0], phandle->rx_max_size);
			}
		}
	}
}
		
/**
  * @brief  HmSerialUart16Handle_transmit
  * @param  None
  * @retval None
  */
uint32_t HmSerialUart16_Transmit(HmSerialUart16Handle_t *phandle, void *pdata, uint32_t len)
{
	uint8_t status = HM_SERIAL_UART16_STATUS_ERROR;
	if(phandle != NULL && phandle->huart != NULL && pdata != NULL)
	{
		if(phandle->mode & HM_SERIAL_UART16_TX_IT_MODE 
			|| phandle->mode & HM_SERIAL_UART16_TX_DMA_MODE)
		{
			uint16_t *p = (uint16_t*)pdata;
			int i = 0;
			while(i < len)
			{
				if((phandle->tx_tail + 1 == phandle->tx_head) || 
				((phandle->tx_tail + 1 == phandle->tx_max_size) && (phandle->tx_head == 0))) // full tx buffer
				{
					/* Start Transfer */
					if(phandle->tx_state == HM_SERIAL_UART16_TX_READY)
					{
						// start transmit event
						HmSerialUart16_LL_MspTxStart(phandle, 1);
						
						uint8_t status = hmSerialUartContinueTx(phandle);
						if(HM_SERIAL_UART16_STATUS_OK == status)
						{
							phandle->tx_state = HM_SERIAL_UART16_TX_BUSY;
						}
					}
					
					// Delay 1 ms for free tx buffer
					HmSerialUart16_Delay(1);
				}
				else
				{
					phandle->tx_bufs[phandle->tx_tail++] = p[i++];
					if(phandle->tx_tail == phandle->tx_max_size)
					{
						phandle->tx_tail = 0;
					}
				}
			}
			
			/* Start Transfer */
			if(phandle->tx_state == HM_SERIAL_UART16_TX_READY)
			{
				// start transmit event
				HmSerialUart16_LL_MspTxStart(phandle, 1);
				
				uint8_t status = hmSerialUartContinueTx(phandle);
				if(HM_SERIAL_UART16_STATUS_OK == status)
				{
					phandle->tx_state = HM_SERIAL_UART16_TX_BUSY;
				}
			}
			
			status = HM_SERIAL_UART16_STATUS_OK;
		}
		else if(phandle->mode & HM_SERIAL_UART16_TX_NORMAL_MODE)
		{
			HAL_StatusTypeDef st = HAL_UART_Transmit(phandle->huart, (uint8_t*)pdata, len, 1);
			if(HAL_OK == st)
			{
				status = HM_SERIAL_UART16_STATUS_OK;
			}
		}
	}
	
	return status;
}

/**
  * @brief  HmSerialUart16_RxAvailable
  * @param  None
  * @retval None
  */
uint32_t HmSerialUart16_RxAvailable(HmSerialUart16Handle_t *phandle)
{
	uint32_t size = 0;
	if(phandle != NULL && phandle->huart)
	{
		/* Interrupt Transfer */
		if(phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
		{
			size = ((phandle->rx_tail + phandle->rx_max_size - phandle->rx_head) 
							% phandle->rx_max_size);
		}
		/* DMA Transfer */
		else if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE)
		{
			phandle->rx_tail = (phandle->rx_max_size - __HAL_DMA_GET_COUNTER(phandle->huart->hdmarx)) 
							% phandle->rx_max_size;
							
			size = ((phandle->rx_tail + phandle->rx_max_size - phandle->rx_head) 
							% phandle->rx_max_size);
		}
	}

	return size;
}

/**
  * @brief  HmSerialUart16_RxFlush
  * @param  None
  * @retval None
  */
void HmSerialUart16_RxFlush(HmSerialUart16Handle_t *phandle)
{
	if(phandle != NULL && phandle->huart != NULL)
	{
		if(phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
		{
			phandle->rx_head = phandle->rx_tail;
		}
		else if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE)
		{
			phandle->rx_tail = (phandle->rx_max_size - __HAL_DMA_GET_COUNTER(phandle->huart->hdmarx)) 
							% phandle->rx_max_size;
							
			phandle->rx_head = phandle->rx_tail;
		}
	}
}

/**
  * @brief  HmSerialUart16Handle_txFlush
  * @param  None
  * @retval None
  */
void HmSerialUart16_TxFlush(HmSerialUart16Handle_t *phandle)
{
	if(phandle != NULL && phandle->huart != NULL)
	{
		if(HM_SERIAL_UART16_TX_READY == phandle->tx_state)
		{
			phandle->tx_head = 0;
			phandle->tx_tail = 0;
		}
		else
		{
			phandle->tx_head = phandle->tx_tail;
			phandle->tx_size = 0;
		}
	}
}

/**
  * @brief  HmSerialUart16_Receive
  * @param  None
  * @retval None
  */
uint32_t HmSerialUart16_Receive(HmSerialUart16Handle_t *phandle, void *pdata, uint32_t *plen)
{
	uint32_t status = HM_SERIAL_UART16_STATUS_ERROR;

	if(phandle != NULL && phandle->huart != NULL)
	{
		uint16_t *p = (uint16_t*)pdata;
		
		/* Interrupt Receive or DMA Receive */
		if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE 
			|| phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
		{
			uint32_t size = HmSerialUart16_RxAvailable(phandle);
			
			size = ((size > *plen) ? *plen : size);
			
			for(int i = 0; i < size; i++) 
			{
				p[i] = phandle->rx_bufs[phandle->rx_head];
				if(++phandle->rx_head == phandle->rx_max_size) 
				{
					phandle->rx_head = 0;
				}
			}
			
			*plen = size;
			
			status = HM_SERIAL_UART16_STATUS_OK;
		}
		else if(phandle->mode & HM_SERIAL_UART16_TX_NORMAL_MODE)
		{
			HAL_StatusTypeDef st = HAL_UART_Receive(phandle->huart, (uint8_t*)pdata, *plen, 1);
			if(HAL_OK == st)
			{
				status = HM_SERIAL_UART16_STATUS_OK;
			}
		}
	}
	
	return status;
}

/**
  * @brief  HmSerialUart16_BackGroundProcess
  * @param  None
  * @retval None
  */
void HmSerialUart16_BackGroundProcess(HmSerialUart16Handle_t *phandle)
{
	if(phandle != NULL && phandle->huart != NULL)
	{
		/* Interrupt Receive or DMA Receive */
		if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE 
			|| phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
		{
			uint32_t count = HmSerialUart16_RxAvailable(phandle);
			if( count != 0)
			{
				if(phandle->rx_wait_count == count)
				{
					uint32_t tmount = HmSerialUart16_GetDeltaTick(phandle->rx_wait_tick);
					
					if(tmount > 100)
					{
						phandle->rx_wait_tick = HmSerialUart16_GetTick();

						phandle->events |= HM_SERIAL_UART16_RX_TIMEOUT_EVENT;
						if(phandle->cb_func)
						{
							uint8_t events = phandle->events;
							phandle->events = phandle->cb_func(phandle, events);
						}

						// reset count
						phandle->rx_wait_count = 0;
					}
				}
				else
				{
					phandle->rx_wait_count = count;
					phandle->rx_wait_tick = HmSerialUart16_GetTick();
				}
			}

		}
	}
}

/**
  * @brief  HmSerialUart16_StartReceiveEvent
  * @param  None
  * @retval None
  */
void HmSerialUart16_StartReceiveEvent(HmSerialUart16Handle_t *phandle, uint32_t time0, uint32_t time1)
{
	if(phandle != NULL && phandle->huart != NULL)
	{
		HmSerialUart16_RxFlush(phandle);
		
		phandle->rx_wait_time_0 = time0;
		phandle->rx_wait_time_1 = time1;
		phandle->rx_wait_tick = HmSerialUart16_GetTick();
		phandle->rx_wait_state = HM_SERIAL_UART16_RX_WAIT_STATE_1;
	}
}

/**
  * @brief  HmSerialUart16_WaitForReceiveEvent
  * @param  None
  * @retval None
  */
uint32_t HmSerialUart16_WaitForReceiveEvent(HmSerialUart16Handle_t *phandle)
{
	uint8_t status = HM_SERIAL_UART16_STATUS_ERROR;
	
	if(phandle != NULL && phandle->huart != NULL)
	{
		status = HM_SERIAL_UART16_STATUS_BUSY;
		switch(phandle->rx_wait_state)
		{
			case HM_SERIAL_UART16_RX_WAIT_STATE_1:
			{
				if(HmSerialUart16_RxAvailable(phandle) == 0)
				{
					uint32_t tmount = HmSerialUart16_GetDeltaTick(phandle->rx_wait_tick);
					if(tmount > phandle->rx_wait_time_0)
					{
						status = HM_SERIAL_UART16_STATUS_TIMEOUT;
						phandle->rx_wait_state = HM_SERIAL_UART16_RX_WAIT_STATE_IDLE;
						phandle->rx_wait_tick = HmSerialUart16_GetTick();
					}
				}
				else
				{
					phandle->rx_wait_count = HmSerialUart16_RxAvailable(phandle);
					phandle->rx_wait_state = HM_SERIAL_UART16_RX_WAIT_STATE_2;
				}
			}
			break;
			
			case HM_SERIAL_UART16_RX_WAIT_STATE_2:
			{
				if(phandle->rx_wait_count == HmSerialUart16_RxAvailable(phandle))
				{
					uint32_t tmount = HmSerialUart16_GetDeltaTick(phandle->rx_wait_tick);
					if(tmount > phandle->rx_wait_time_1)
					{
						status = HM_SERIAL_UART16_STATUS_OK;
						phandle->rx_wait_tick = HmSerialUart16_GetTick();
						phandle->rx_wait_state = HM_SERIAL_UART16_RX_WAIT_STATE_IDLE;
					}
				}
				else
				{
					phandle->rx_wait_tick = HmSerialUart16_GetTick();
					phandle->rx_wait_count = HmSerialUart16_RxAvailable(phandle);
				}
			}
			break;
		}
	}
	
	return status;
}

/**
  * @brief  HmSerialUart16_WaitForReceive
  * @param  None
  * @retval None
  */
uint32_t HmSerialUart16_WaitForReceive(HmSerialUart16Handle_t *phandle, uint32_t  time0, uint32_t time1)
{
	uint32_t status = HM_SERIAL_UART16_STATUS_ERROR;
	
	if(phandle != NULL && phandle->huart != NULL)
	{
		status = HM_SERIAL_UART16_STATUS_TIMEOUT;
		if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE 
			|| phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
		{
			phandle->rx_wait_time_0 = time0;
			phandle->rx_wait_time_1 = time1;
			phandle->rx_wait_count = 0;
			phandle->rx_wait_tick = HmSerialUart16_GetTick();
			uint32_t tmount = 0;
															
			while(tmount < phandle->rx_wait_time_0)
			{
				phandle->rx_wait_count = HmSerialUart16_RxAvailable(phandle);
				if(phandle->rx_wait_count != 0)
				{
					phandle->rx_wait_tick = HmSerialUart16_GetTick();
					break;
				}
				else
				{
					HAL_Delay(1);
					tmount = HmSerialUart16_GetDeltaTick(phandle->rx_wait_tick);
				}
			}

			// available
			if(phandle->rx_wait_count != 0)
			{
				tmount = 0;
				while(tmount < phandle->rx_wait_time_1)
				{
					uint32_t count = HmSerialUart16_RxAvailable(phandle);
					if(phandle->rx_wait_count != count)
					{
						phandle->rx_wait_count = count;
						phandle->rx_wait_tick = HmSerialUart16_GetTick();
					}
					else
					{
						HAL_Delay(1);
						tmount = HmSerialUart16_GetDeltaTick(phandle->rx_wait_tick);
					}
				}

				status = HM_SERIAL_UART16_STATUS_OK;
			}
		}
	}
	
	return status;
}

/**
  * @brief  HmSerialUartCpltCallback
  * @param  None
  * @retval None
  */
void HmSerialUart16_TxCpltCb(HmSerialUart16Handle_t *phandle)
{
	phandle->tx_head += phandle->tx_size;
	phandle->tx_size = 0;
	
	if(phandle->tx_head == phandle->tx_max_size)
	{
		phandle->tx_head = 0;
	}

	if(phandle->tx_head != phandle->tx_tail)
	{
		hmSerialUartContinueTx(phandle);
	}
	else
	{
		phandle->tx_state = HM_SERIAL_UART16_TX_READY;
		
		// stop transmit event
		HmSerialUart16_LL_MspTxStart(phandle, 0);
	
		/* Empty Tx Buffer Event */
		phandle->events |= HM_SERIAL_UART16_TX_EMPTY_EVENT;
		if(phandle->cb_func)
		{
			uint8_t events = phandle->events;
			phandle->events = phandle->cb_func(phandle, events);
		}
	}
}

/**
  * @brief  HmSerialUart16_ErrorCb
  * @param  None
  * @retval None
  */
void HmSerialUart16_ErrorCb(HmSerialUart16Handle_t *phandle)
{

}

/**
  * @brief  HmSerialUart16_RxCpltCb
  * @param  None
  * @retval None
  */
void HmSerialUart16_RxCpltCb(HmSerialUart16Handle_t *phandle)
{
	/* Interrupt Receive */
	if(phandle->mode & HM_SERIAL_UART16_RX_IT_MODE)
	{
		if((phandle->rx_tail + 1 == phandle->rx_head) || 
			((phandle->rx_tail + 1 == phandle->rx_max_size) && (phandle->rx_head == 0)))
		{
			// do nothing because rx buffer is full
		}
		else
		{
			if(++phandle->rx_tail == phandle->rx_max_size)
			{
				phandle->rx_tail = 0;
			}
		}

		HAL_UART_Receive_IT(phandle->huart, (uint8_t*)&phandle->rx_bufs[phandle->rx_tail], 1);
	}
	/* DMA Receive */
	else if(phandle->mode & HM_SERIAL_UART16_RX_DMA_MODE)
	{
		HAL_UART_Receive_DMA(phandle->huart, (uint8_t*)&phandle->rx_bufs[0], phandle->rx_max_size);
		phandle->rx_tail = 0;
	}
}

/**
  * @brief  hmSerialUartContinueTx
  * @param  None
  * @retval ERROR: size = 0; BUSY: BUSY; OK: Tranfer
  */
static uint32_t hmSerialUartContinueTx(HmSerialUart16Handle_t *phandle)
{
	uint32_t status = HM_SERIAL_UART16_STATUS_ERROR;
	uint32_t size = HM_SERIAL_UART16_TX_ALIGNED_LEN(phandle);
	
	uint16_t *p = (uint16_t*)&phandle->tx_bufs[phandle->tx_head];

	if(phandle->mode & HM_SERIAL_UART16_TX_IT_MODE) /* Interrupt Transfer */
	{
		HAL_StatusTypeDef st = HAL_UART_Transmit_IT(phandle->huart, (uint8_t*)p, size);
		
		/* Check for status */
		if(HAL_OK == st)
		{
			phandle->tx_size = size;
			status = HM_SERIAL_UART16_STATUS_OK;
		}
	}
	else if(phandle->mode & HM_SERIAL_UART16_TX_DMA_MODE) /* DMA Transfer */
	{
		HAL_StatusTypeDef st = HAL_UART_Transmit_DMA(phandle->huart, (uint8_t*)p, size);
		
		/* Check for status */
		if(HAL_OK == st)
		{
			phandle->tx_size = size;
			status = HM_SERIAL_UART16_STATUS_OK;
		}
	}
	
	return status;
}

