/*
	MrHome board definition include header file

	HieuNT
	23/3/2017
*/

#ifndef __MRHOME_BOARD_DEF_H_
#define __MRHOME_BOARD_DEF_H_

#include "stm32l151cb_si4463_board.h"
#include "stm32l1xx_hal.h"
#include "stm32l1xx_flash_if.h"

#endif
