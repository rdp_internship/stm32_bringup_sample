/*
	BSP (Board support package)
	For HieuNT STM32L151CB and Si4463 board

	HieuNT
	11/8/2016
*/

#ifndef __STM32L151CB_SI4463_BOARD_H_
#define __STM32L151CB_SI4463_BOARD_H_

#include <stdbool.h>

#include "main.h" // ~ bsp pin def. from cubemx
#include "gpio.h"

#define BSP_LED_ON() HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_SET);
#define BSP_LED_OFF() HAL_GPIO_WritePin(USER_LED_GPIO_Port, USER_LED_Pin, GPIO_PIN_RESET);

#define BSP_Si446x_CS_HIGH()	HAL_GPIO_WritePin(SPI1_SEL_GPIO_Port, SPI1_SEL_Pin, GPIO_PIN_SET);
#define BSP_Si446x_CS_LOW()		HAL_GPIO_WritePin(SPI1_SEL_GPIO_Port, SPI1_SEL_Pin, GPIO_PIN_RESET);
#define BSP_Si446x_ShutDown()	HAL_GPIO_WritePin(SPI1_SDN_GPIO_Port, SPI1_SDN_Pin, GPIO_PIN_SET);
#define BSP_Si446x_Active()		HAL_GPIO_WritePin(SPI1_SDN_GPIO_Port, SPI1_SDN_Pin, GPIO_PIN_RESET);
#define BSP_Si446x_CTS()		HAL_GPIO_ReadPin(SPI1_GPIO0_GPIO_Port, SPI1_GPIO0_Pin)
#define BSP_Si446x_nIRQ()		HAL_GPIO_ReadPin(SPI1_IRQ_GPIO_Port, SPI1_IRQ_Pin)

uint8_t BSP_SPI_ReadWrite(uint8_t byte);
void BSP_SPI_WriteData(uint8_t byteCount, uint8_t *pData);
void BSP_SPI_ReadData(uint8_t byteCount, uint8_t *pData);

// Si446x SPI porting
void BSP_IO_Init(void);
#define BSP_Si446x_ReadWrite(byteToWrite) BSP_SPI_ReadWrite(byteToWrite)
#define BSP_Si446x_WriteData(byteCount, pData) BSP_SPI_WriteData(byteCount, pData)
#define BSP_Si446x_ReadData(byteCount, pData) BSP_SPI_ReadData(byteCount, pData)

#define START_TICK()	// TIM4_Cmd(ENABLE)
#define STOP_TICK()		// TIM4_Cmd(DISABLE)
#define RESTART_TICK()	// do {STOP_TICK(); tick = 0; START_TICK();} while(0)

#define SERVO_RUN(duty)	// do { TIM2->CCR3H = (uint8_t)(((40250*duty/100) >> 8) & 0xFF); \
  							// TIM2->CCR3L = (uint8_t)((40250*duty/100) & 0xFF);} while (0)

#define EEPROM_Write(address, data) // FLASH_ProgramByte((uint32_t)(address), (uint8_t)(data))
#define EEPROM_Read(address) // FLASH_ReadByte((uint32_t)(address))

extern __IO bool pirExtIntActive;
extern __IO bool btnExtIntActive;

#endif
