/**
  ******************************************************************************
  * @file    USB_Device/DFU_Standalone/Src/usbd_dfu_flash.c
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    31-July-2015
  * @brief   Memory management layer
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2,(the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#define __DEBUG__ 4
#ifndef __MODULE__
	#define __MODULE__ ""
#endif

#include "stm32l1xx_flash_if.h"
#include "stm32l1xx_hal_conf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Extern function prototypes ------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initializes Memory.
  * @param  None
  * @retval 0 if operation is successeful, MAL_FAIL else.
  */
void Flash_If_Init(void)
{ 
	/* Unlock the internal flash */  
	HAL_FLASH_Unlock();
}

/**
  * @brief  De-Initializes Memory.
  * @param  None
  * @retval 0 if operation is successeful, MAL_FAIL else.
  */
void Flash_If_DeInit(void)
{ 
	/* Lock the internal flash */
	HAL_FLASH_Lock();
}

/**
  * @brief  Erases sector.
  * @param  Add: Address of sector to be erased.
  * @retval 0 if operation is successeful, MAL_FAIL else.
  */
void Flash_If_Erase()
{
	uint32_t NbOfPages = 0;
	uint32_t PageError = 0;
	FLASH_EraseInitTypeDef eraseinitstruct;

	/* Get the number of sector to erase from 1st sector*/
	NbOfPages =((FLASH_IF_END_ADD - FLASH_IF_START_ADD) / FLASH_PAGE_SIZE);
	eraseinitstruct.TypeErase = FLASH_TYPEERASE_PAGES;
	eraseinitstruct.PageAddress = FLASH_IF_START_ADD;
	eraseinitstruct.NbPages = NbOfPages;
	HAL_FLASHEx_Erase(&eraseinitstruct, &PageError);
}

// addr: start pasge
// nums number of pages
void Flash_If_EraseStart(uint32_t start_addr, uint32_t end_addr)
{
	if(end_addr < start_addr)
	{
		return;
	}
	
	uint32_t NbOfPages = 0;
	uint32_t PageError = 0;

	/* Variable contains Flash operation status */
	FLASH_EraseInitTypeDef eraseinitstruct;

	/* Get the number of sector to erase from 1st sector*/
	NbOfPages = (end_addr - start_addr) / FLASH_PAGE_SIZE;
	eraseinitstruct.TypeErase = FLASH_TYPEERASE_PAGES;
	eraseinitstruct.PageAddress = start_addr;
	eraseinitstruct.NbPages = NbOfPages;
	HAL_FLASHEx_Erase(&eraseinitstruct, &PageError);
}

/**
  * @brief  Writes Data into Memory.
  * @param  psrc: dia chi cua sram.
  * @param  dest: dia chi cua flash.
  * @param  len: Number of data to be written(in bytes).
  * @retval FLASH_IF_SUCCESS if operation is success, FLASH_IF_ERROR else.
  */
uint32_t Flash_If_Write(uint8_t *psrc, uint32_t dest, uint32_t len)
{
	uint32_t status = FLASH_IF_ERROR;
	for(int i = 0; i < len; i += 4)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by byte */ 
		if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,(uint32_t)(dest + i), *(uint32_t*)(psrc + i)) == HAL_OK)
		{
			/* Check the written value */
			if(*(uint32_t *)(psrc + i) != *(uint32_t*)(dest + i))
			{
				/* Flash content doesn't match SRAM content */
				return status;
			}
		}
		else
		{
			/* Error occurred while writing data in Flash memory */
			return status;
		}
	}
	
	return FLASH_IF_SUCCESS;
}

/**
  * @brief  Reads Data into Memory.
  * @param  src: Pointer to the source buffer. Address to be written to.
  * @param  dest: Pointer to the destination buffer.
  * @param  len: Number of data to be read(in bytes).
  * @retval Pointer to the physical address where data should be read.
  */
void Flash_If_Read(uint8_t *psrc, uint8_t *pdst, uint32_t len)
{
	for(int i = 0; i < len; i++)
	{
		pdst[i] = psrc[i];
	}
}

/************************(C) COPYRIGHT STMicroelectronics *****END OF FILE****/
