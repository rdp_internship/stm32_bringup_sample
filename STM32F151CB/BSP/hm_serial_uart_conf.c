
#define __DEBUG__ 4
#ifndef __MODULE__
	#define __MODULE__ ""
#endif
#include "debug.h"

#include "hm_serial_uart_conf.h"
#include "hm_serial_uart8.h"
#include "usart.h"

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	// debug + main + sim uart
	HmSerialUart8_TxCpltCb(huart->pData);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	// debug + main + sim uart
	HmSerialUart8_RxCpltCb(huart->pData);
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	// debug + main + sim uart
	HmSerialUart8_ErrorCb(huart->pData);
}
