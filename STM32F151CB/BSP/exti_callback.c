/**
	EXTI callback implementation source

	HieuNT
	15/12/2016
*/

#define __DEBUG__ 0
#ifndef __MODULE__
	#define __MODULE__ "exti_callback.c"
#endif
#include "debug.h"

// #include "bsp.h"

#include "mrhome_board.h"
#include <stdint.h>
#include <stdbool.h>

// extern bool Si446x_nIRQ_Active;
extern __IO bool pirExtIntActive;
extern __IO bool btnExtIntActive;

/* =================== IRQ Callback handler ===================== */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch (GPIO_Pin){
    case GPIO_PIN_0:
      //Si446x_nIRQ_Active = TRUE;
      break;
    case GPIO_PIN_4:
    	pirExtIntActive = true;
    	break;
    case GPIO_PIN_10:
      btnExtIntActive = true;
      break;
  }
}
